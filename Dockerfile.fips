# When updating version make sure to check on semgrepignore file as well
ARG SCANNER_VERSION=1.50.0
ARG POST_ANALYZER_SCRIPTS_VERSION=0.2.0
ARG TRACKING_CALCULATOR_VERSION=2.4.1
ARG VET_VERSION=0.18.3
ARG STENCILS_VERSION=0.2.0

FROM registry.gitlab.com/security-products/post-analyzers/scripts:${POST_ANALYZER_SCRIPTS_VERSION} AS scripts
FROM registry.gitlab.com/security-products/post-analyzers/tracking-calculator:${TRACKING_CALCULATOR_VERSION} AS tracking
FROM registry.gitlab.com/security-products/vet:${VET_VERSION} AS vet
FROM registry.gitlab.com/gitlab-org/security-products/vet/stencils:${STENCILS_VERSION} AS recipes

FROM registry.gitlab.com/gitlab-org/gitlab-runner/go-fips:1.18 AS build

ENV CGO_ENABLED=0 GOOS=linux

WORKDIR /go/src/buildapp
COPY . .

# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o /analyzer-semgrep

FROM registry.access.redhat.com/ubi8-minimal
USER root

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION}
ENV SEMGREP_R2C_INTERNAL_EXPLICIT_SEMGREPIGNORE "/semgrepignore"
ENV VET_CONFIGURATION_FILE="/verify/semgrep.toml"
ENV SAST_RULES_PROJECTID=27038823
ENV SAST_RULES_VERSION=2.0.2

# Run VET FP reduction only on Go files
ENV VET_LANG_EXT=".go"

COPY --from=build /analyzer-semgrep /analyzer-binary
COPY semgrepignore /semgrepignore

RUN microdnf update --disableplugin=subscription-manager --nodocs && \
    microdnf install shadow-utils util-linux git wget unzip python39 --disableplugin=subscription-manager --nodocs && \
    \
    # Install Semgrep
    pip3 install semgrep==$SCANNER_VERSION && \
    \
    # Configure CA Certificates
    # Ref: https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/security_guide/sec-shared-system-certificates
    \
    mkdir -p /etc/pki/ca-trust/source/anchors/ && \
    ## Change the certs owner to the user
    touch /etc/pki/ca-trust/source/anchors/ca-certificates.crt && \
    \
    ## Change the certs owner to the user
    chmod -R g+w /etc/pki/ && \
    \
    # Create gitlab user
    useradd --create-home gitlab -g root && \
    \
    # Cleanup libs that are no longer needed
    microdnf remove shadow-utils && \
    microdnf clean all
    

RUN wget https://gitlab.com/api/v4/projects/${SAST_RULES_PROJECTID}/packages/generic/sast-rules/v${SAST_RULES_VERSION}/sast-rules.zip && \
    unzip sast-rules.zip && \
    rm sast-rules.zip && \
    mkdir -p rules && \
    cp dist/eslint.yml /rules && \
    cp dist/find_sec_bugs_scala.yml /rules && \
    cp dist/flawfinder.yml /rules && \
    cp dist/find_sec_bugs.yml /rules && \
    cp dist/security_code_scan.yml /rules && \
    cp dist/gosec.yml /rules && \
    cp dist/bandit.yml /rules

RUN microdnf remove unzip wget microdnf 

COPY --from=tracking /analyzer-tracking /analyzer-tracking
COPY --from=scripts /start.sh /analyzer
COPY --from=vet /usr/bin/analyzer /vet
COPY --from=recipes /config/verify /verify

USER gitlab

ENTRYPOINT []
CMD ["/analyzer", "run"]
